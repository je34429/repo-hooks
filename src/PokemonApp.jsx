import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import { getPokemons } from "./store/slices/pokemon";


export const PokemonApp = () => {

  // const estado = useSelector(state=> state)
  // console.log('estado', estado);

  const  {pokemons, page, isLoading}  = useSelector(state=> state.pokemons);
  
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getPokemons());
  }, [])
  
  return (
      <>
        <h1>PokemonApp</h1>
        <hr/>
        <span>Loading:{isLoading ? 'True':'false'}</span>
        <ul>
          {pokemons.map((pokemon, i)=>(
            <li key={pokemon.name+i}>{pokemon.name}</li>
          ))}
            
        </ul>
        <button
          disabled={isLoading}
          onClick={() => dispatch(getPokemons(page))}
        >
          Next page
        </button>

      </>
  )
}
